// Tugas Day 9 Week 2 SanberCode ReactNative
// Luthfi Naufan Yamin
// luthfiny@gmail.com
// es6.js

const cl =  (x) => console.log(x);

// Soal 1
cl("Soal 1")
const golden =  () => console.log("this is golden!!");

golden()

// Soal 2
cl("\nSoal 2")
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => console.log(`${firstName} ${lastName}`)
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName()

// Soal 3
cl("\nSoal 3")
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

// Driver code
const {firstName, lastName, destination, occupation} = newObject;
console.log(firstName, lastName, destination, occupation)

// Soal 4
cl("\nSoal 4")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
//Driver Code
const combined = [...west, ...east];
console.log(combined)

// Soal 5
cl("\nSoal 5")

const planet = "earth"
const view = "glass"
// var before = 'Lorem ' + view + 'dolor sit amet, ' +  
//     'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam'
 
// Driver Code
var before = `Lorem ${view} dolor sit amet,  ` +  
    `consectetur adipiscing elit ${planet} do eiusmod tempor` +
    `incididunt ut labore et dolore magna aliqua. Ut enim` +
    ` ad minim veniam`
console.log(before) 