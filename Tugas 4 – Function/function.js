// Tugas Day 4 Week 1 SanberCode ReactNative
// Luthfi Naufan Yamin
// luthfiny@gmail.com
// function.js

let cl = (x) => console.log(x);

// Soal 1
cl("Soal 1");

let teriak = ()=> "Halo Sanbers!";
 
console.log(teriak()) // "Halo Sanbers!" 

// Soal 2
cl("\nSoal 2");

let kalikan = (x, y)=> x*y;

var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

// Soal 3
cl("\nSoal 3");

let introduce = (nama, umur, almt, hobi)=> `Nama saya ${nama}, umur saya ${umur} tahun, alamat saya di ${almt}, dan saya punya hobby yaitu ${hobi}!`;

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 
