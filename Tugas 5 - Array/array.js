// Tugas Day 5 Week 1 SanberCode ReactNative
// Luthfi Naufan Yamin
// luthfiny@gmail.com
// array.js

// Soal 1
function range(startNum, finishNum) {
    var rangeRes = [];
    if (finishNum > startNum) {
        for (var i = startNum; i <= finishNum; i++) {
            rangeRes.push(i);
        }
        return rangeRes;
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i--) {
            rangeRes.push(i);
        }
        return rangeRes;
    } else {
        return -1;
    }
}

// Code di sini
console.log("Soal 1") 
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal 2
function rangeWithStep(startNum, finishNum, step = 1) {
    var rangeStepRes = [];
    if (finishNum > startNum) {
        for (var i = startNum; i <= finishNum; i+= step) {
            rangeStepRes.push(i);
        }
        return rangeStepRes;
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i-= step) {
            rangeStepRes.push(i);
        }
        return rangeStepRes;
    } else {
        return -1;
    }    
}

// Code di sini
console.log("\nSoal 2")
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal 3
function arraySum(array2Sum) {
    var sum = 0;
    for (var i = 0; i < array2Sum.length; i++) {
        sum += array2Sum[i];
    }
    return sum;
} 

function sum(startNum, finishNum = null, step = 1) {
    var rangeStepRes = [];
    if (finishNum > startNum) {
        for (var i = startNum; i <= finishNum; i+= step) {
            rangeStepRes.push(i);
        }
        return arraySum(rangeStepRes);
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i-= step) {
            rangeStepRes.push(i);
        }
        return arraySum(rangeStepRes);
    } else if (finishNum = null) {
        return startNum;
    } else {
        return 0;
    }
}

// Code di sini
console.log("\nSoal 3")
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal 4
//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(array2dimensi) {
    for (var i = 0; i < array2dimensi.length; i++) {
        console.log(`Nomor ID: ${array2dimensi[i][0]}`)
        console.log(`Nama Lengkap: ${array2dimensi[i][1]}`)
        console.log(`TTL: ${array2dimensi[i][2]} ${array2dimensi[i][3]}`)
        console.log(`Hobi: ${array2dimensi[i][4]}\n`)
    }
}

console.log("\nSoal 4")
dataHandling(input)

// Soal 5

function balikKata(inputString) {
    var resString = '';
    for (var i = (inputString.length - 1); i >= 0; i--) {
        resString += inputString[i];
    }
    return resString;
}

// Code di sini
console.log("\nSoal 5")
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

// Soal 6
console.log("\nSoal 6")
function cekBulan(angkaBulan) {
    switch (angkaBulan) {
        case "01" : return "Januari";
        case "02" : return "Februari";
        case "03" : return "Maret";
        case "04" : return "April";
        case "05" : return "Mei";
        case "06" : return "Juni";
        case "07" : return "Juli";
        case "08" : return "Agustus";
        case "09" : return "September";
        case "10" : return "Oktober";
        case "11" : return "November";
        case "12" : return "Desember";
        default : return "Bulan invalid";
    } 
}

function dataHandling2(inputArray) {
    // Poin 1
    inputArray.splice(1, 1, inputArray[1]+"Elsharawy");
    inputArray.splice(2, 1, "Provinsi "+inputArray[2]);
    inputArray.splice(4, 2,  "Pria", "SMA Internasional Metro");
    console.log(inputArray);
    // Poin 2
    var arrayDate = inputArray[3].split('/');
    console.log(cekBulan(arrayDate[1]));
    // Poin 3
    console.log(arrayDate.sort(function(a, b){return b-a}));
    // Poin 4
    console.log(inputArray[3].split('/').join('-'));
    // Poin 5
    console.log(inputArray[1].slice(0, 15));
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);