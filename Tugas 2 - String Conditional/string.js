// Tugas Day 2 SanberCode ReactNative
// Luthfi Naufan Yamin
// luthfiny@gmail.com
// string.js

// Soal No. 1 (Membuat kalimat)
console.log("Nomor 1");
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var kalimat = word+" "+second+" "+third+" "+fourth+" "+fifth+" "+sixth+" "+seventh;
console.log(kalimat);

// Soal No.2 Mengurai kalimat (Akses karakter dalam string),
console.log("\nNomor 2");
var sentence = "I am going to be React Native Developer"; 
var words = sentence.split(" ");

var exampleFirstWord = sentence[0] ; 
var exampleSecondWord = sentence[2] + sentence[3]  ; 
var secondWord = words[1]; // lakukan sendiri 
var thirdWord = words[2]; // lakukan sendiri 
var fourthWord = words[3]; // lakukan sendiri 
var fifthWord = words[4]; // lakukan sendiri 
var sixthWord = words[5]; // lakukan sendiri 
var seventhWord = words[6]; // lakukan sendiri 
var eighthWord = words[7]; // lakukan sendiri 

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)

// Soal No. 3 Mengurai Kalimat (Substring)
console.log("\nNomor 3");
var sentence2 = 'wow JavaScript is so cool'; 
var words2 = sentence2.split(" ");

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = words2[1]; // do your own! 
var thirdWord2 = words2[2]; // do your own! 
var fourthWord2 = words2[3]; // do your own! 
var fifthWord2 = words2[4]; // do your own! 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

// Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String
console.log("\nNomor 4");
var sentence3 = 'wow JavaScript is so cool'; 
var words3 = sentence3.split(" ");

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = words3[1]; // do your own! 
var thirdWord3 = words3[2]; // do your own! 
var fourthWord3 = words3[3]; // do your own! 
var fifthWord3 = words3[4]; // do your own! 

var firstWordLength = exampleFirstWord3.length  
// lanjutkan buat variable lagi di bawah ini 
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWord3.length); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWord3.length); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWord3.length); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWord3.length); 
