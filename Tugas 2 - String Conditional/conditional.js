// Tugas Day 2 SanberCode ReactNative
// Luthfi Naufan Yamin
// luthfiny@gmail.com
// conditional.js

// Nomor 1 if-else
var nama = "Luthfi";
var peran = "Werewolf";

if (nama == null) {
    // Output untuk Input nama = '' dan peran = ''
    console.log("Nama harus diisi!");
} else if (peran == null) {
    //Output untuk Input nama = isi dan peran = ''
    console.log("Halo "+ nama +", Pilih peranmu untuk memulai game!");
} else if (peran == "Penyihir") {
    //Output untuk Input nama isi dan peran 'Penyihir'
    console.log("Selamat datang di Dunia Werewolf, "+ nama);
    console.log("Halo "+ peran +" "+ nama +", kamu dapat melihat siapa yang menjadi werewolf!");
} else if (peran == "Guard") {
    //Output untuk Input nama isi dan peran 'Guard'
    console.log("Selamat datang di Dunia Werewolf, "+ nama);
    console.log("Halo "+ peran +" "+ nama +", kamu akan membantu melindungi temanmu dari serangan werewolf.");
} else if (peran == "Werewolf") {
    //Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
    console.log("Selamat datang di Dunia Werewolf, "+ nama);
    console.log("Halo "+ peran +" "+ nama +", Kamu akan memakan mangsa setiap malam!");
} else {
    //Output untuk Input nama = isi dan peran invalid
    console.log("Halo "+ nama +", Pilih peran Penyihir, Guard, atau Werewolf untuk memulai game!");
}

// Nomor 2 switch case
var tanggal = 19; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 6; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1996; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

var namaBulan;
switch(bulan) {
    case 1:   { namaBulan = 'Januari';  console.log(String(tanggal) + ' ' + namaBulan + ' ' + String(tahun)); break; }
    case 2:   { namaBulan = 'Februari'; console.log(String(tanggal) + ' ' + namaBulan + ' ' + String(tahun)); break; }
    case 3:   { namaBulan = 'Maret';    console.log(String(tanggal) + ' ' + namaBulan + ' ' + String(tahun)); break; }
    case 4:   { namaBulan = 'April';    console.log(String(tanggal) + ' ' + namaBulan + ' ' + String(tahun)); break; }
    case 5:   { namaBulan = 'Mei';      console.log(String(tanggal) + ' ' + namaBulan + ' ' + String(tahun)); break; }
    case 6:   { namaBulan = 'Juni';     console.log(String(tanggal) + ' ' + namaBulan + ' ' + String(tahun)); break; }
    case 7:   { namaBulan = 'Juli';     console.log(String(tanggal) + ' ' + namaBulan + ' ' + String(tahun)); break; }
    case 8:   { namaBulan = 'Agustus';  console.log(String(tanggal) + ' ' + namaBulan + ' ' + String(tahun)); break; }
    case 9:   { namaBulan = 'September'; console.log(String(tanggal) + ' ' + namaBulan + ' ' + String(tahun)); break; }
    case 10:   { namaBulan = 'Oktober';  console.log(String(tanggal) + ' ' + namaBulan + ' ' + String(tahun)); break; }
    case 11:   { namaBulan = 'November'; console.log(String(tanggal) + ' ' + namaBulan + ' ' + String(tahun)); break; }
    case 12:   { namaBulan = 'Desember'; console.log(String(tanggal) + ' ' + namaBulan + ' ' + String(tahun)); break; }
    default:  { console.log('Input bulan invalid'); }
}
