// Tugas Day 6 Week 2 SanberCode ReactNative
// Luthfi Naufan Yamin
// luthfiny@gmail.com
// object.js

// Soal 1
console.log('Soal 1');

var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(arr) {
    // Code di sini 
    if (arr == null) {
        return console.log("");
    }
    var keys = ['firstName', 'lastName', 'gender', 'age'];
    var strOut = '';
    for (var i = 0; i < arr.length; i++) {
        // var newMap = new Map(keys, arr[i]);
        var newMap = [];
        for (var j = 0; j < keys.length; j++) {
            newMap.push([keys[j], arr[i][j]]);
        }
        var newObject = Object.fromEntries(newMap);
        if ((newObject.age == undefined) || (newObject.age > thisYear)) {
            newObject.age = "Invalid birth year";
        } else {
            newObject.age = thisYear - newObject.age;
        }
        // console.log(`${i+1}. ${arr[i][0]} ${arr[i][1]} :`+newObject)
        console.log(i+1+'.',newObject.firstName,newObject.lastName+':',newObject);
    }
    // console.log(strOut);
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

// Soal 2
console.log('\nSoal 2');
function shoppingTime(memberId, money = null) {
    // you can only write your code here!
    // if ((memberId == null) || (memberId == '')) {
    if (!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup";
    }
    let objectItem = {
        'Sepatu Stacattu': 1500000,
        'Baju Zoro': 500000,
        'Baju H&N': 250000,
        'Sweater Uniklooh': 175000,
        'Casing Handphone': 50000
    };
    var retObject = {memberId:memberId, money:money, listPurchased:[], changeMoney:money};
    for (let [key, value] of Object.entries(objectItem)) {
        if (retObject.changeMoney >= value) {
            retObject.listPurchased.push(key);
            retObject.changeMoney -= value;
        }
    }
    return retObject;
}
   
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal 3
console.log('\nSoal 3');
function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  //your code here
  var objectArr = [];
  for (var i = 0; i < arrPenumpang.length; i++) {
      var bayar = (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1])) * 2000;
      var addObject = {
        penumpang: arrPenumpang[i][0],
        naikDari: arrPenumpang[i][1],
        tujuan: arrPenumpang[i][2],
        bayar: bayar
      };
      objectArr.push(addObject);
  }
  return objectArr;
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]