// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var timeLeft = 10000;
function callReadBooks(x) {
    if (x >= books.length) {
        console.log("saya sudah membaca semua buku!")
    } else {
        readBooks(timeLeft, books[x], (time)=> {
            timeLeft = time;
            callReadBooks(x+1);
        });
    }
}
callReadBooks(0);