var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var timeLeft = 10000;
function tagihReadPromise(x) {
    if (x >= books.length) {
        console.log("semua buku sudah saya baca!");
    } else {
        readBooksPromise(timeLeft, books[x])
            .then(function (sisaWaktu) {
                timeLeft = sisaWaktu;
                tagihReadPromise(x+1);
            })
            .catch(function (sisaWaktu) {
                console.log(sisaWaktu);
            });
    }
}
tagihReadPromise(0);