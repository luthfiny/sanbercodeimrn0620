// Tugas Day 3 Week 1 SanberCode ReactNative
// Luthfi Naufan Yamin
// luthfiny@gmail.com
// looping.js

let cl = (x) => console.log(x);

// Soal 1
cl("Soal 1");
cl("LOOPING PERTAMA");
let ctr1 = 0;
while (ctr1 < 20) {
    ctr1+= 2;
    cl(`${ctr1} - I love coding`);
}
cl("LOOPING KEDUA");
while(ctr1 > 1) {
    cl(`${ctr1} - I will become a mobile developer`);
    ctr1-= 2;
}

// Soal 2
cl("\nSoal 2");
for (var ctr2 = 1; ctr2 < 20; ctr2++) {
    if (((ctr2 % 2) == 1) && ((ctr2 % 3) == 0)) {
        cl(`${ctr2} - I Love Coding`);
    } else if ((ctr2 % 2) == 1) {
        cl(`${ctr2} - Santai`);
    } else {
        cl(`${ctr2} - Berkualitas`);
    }
}

// Soal 3
cl("\nSoal 3");
let draw = '';
for (var ctr3 = 0; ctr3 < 8; ctr3++) {
    draw += '#';
}
for (var ctr3 = 0; ctr3 < 4; ctr3++) {
    cl(draw)
}

// Soal 4
cl("\nSoal 4");
for (var ctr4 = 1; ctr4 < 8; ctr4++) {
    draw = '';
    for (var i = 0; i < ctr4; i++) {
        draw += '#';
    }
    cl(draw)
}

// Soal 5
cl("\nSoal 5");
let draw1 = '';
let draw2 = '';
for (var ctr5 = 0; ctr5 < 8; ctr5+= 2) {
    draw1 += ' #';
    draw2 += '# ';
}
for (var ctr5 = 0; ctr5 < 8; ctr5+= 2) {
    cl(draw1);
    cl(draw2);
}
